PREFIX ?= /usr/local
CC ?= cc
LDFLAGS = -lX11

output: lemonaid.c config.h
	${CC}  lemonaid.c $(LDFLAGS) -o lemonaid

clean:
	rm -f *.o *.gch lemonaid

install: output
	lxqt-sudo mkdir -p $(DESTDIR)$(PREFIX)/bin
	lxqt-sudo install -m 0755 lemonaid $(DESTDIR)$(PREFIX)/bin/lemonaid

uninstall:
	lxqt-sudo rm -f $(DESTDIR)$(PREFIX)/bin/lemonaid
